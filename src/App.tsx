import React from 'react';
import './main.global.css';
import { hot } from 'react-hot-loader/root';
import { Layout } from './shared/Layout';
import { Header } from './shared/Header';
import { Content } from './shared/Content';
import { CardsList } from './shared/CardsList';

export function AppComponent() {
  return (
    <div>
      <Layout>
        <Header />

        <Content><CardsList /></Content>
      </Layout>
    </div>
  );
}

export const App = hot(AppComponent);
